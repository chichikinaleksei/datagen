<?php
/**
 * Block Name: Home common
 */

?>


  <?php $i = 1; ?>
  <?php if( have_rows('common') ) : ?>
    <?php while( have_rows('common') ) : the_row(); ?>
      <?php if( $i > 1 ) : ?>
        <?php $down = 'common__slide--is-down'; ?>
        <?php $active = ' '; ?>
      <?php else : ?>
        <?php $active = 'active-slide'; ?>
      <?php endif; ?>


      <section class="common common--desktop section" id="common-<?php echo $i; ?>">
        <div class="common__slide">
          <div class="common__container">
            <div
              class="common__img-top"
              style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('image_1', get_the_ID() ), 'common-top', true ); ?>');"
              data-aos="fade-up"
              data-aos-offset="-50"
              data-aos-delay="0"
              data-aos-duration="1000"
              data-aos-easing="ease-in-out"
              data-aos-mirror="true"
              data-aos-once="false">
            </div>
            <div class="container container--narrow">
              <div
                class="common__img-bottom common__img-is-hidden"
                style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('image_2', get_the_ID() ), 'common-bottom', true ); ?>');"

                data-aos="fade-up"
                data-aos-offset="-60"
                data-aos-delay="10"
                data-aos-duration="1200"
                data-aos-easing="ease-in-out"
                data-aos-mirror="true"
                data-aos-once="false">
              </div>
            </div>
            <div class="common__wrapper flex row jcsb aife">
              <div class="block block--common">
                <h2 class="ch-title"><?php echo get_sub_field('title'); ?></h2>
                <?php if( get_sub_field('title_blue') ) : ?>
                  <h2 class="ch-title ch-title--blue"><?php echo get_sub_field('title_blue'); ?></h2>
                <?php endif; ?>
                <p class="ch-text"><?php echo get_sub_field('text'); ?></p>
                <?php if( get_sub_field('more_information') ) : ?>
                <?php
                  if(get_sub_field('open_modal_with_more_information')) {
                    $modal = 'open-modal';
                  } else {
                    $modal = ' ';
                  }
                  ?>
                  <a class="link link--small <?php echo $modal; ?>" href="<?php echo get_sub_field
                  ('more_information'); ?>"
                     target="_blank">
                    <div class="link__wrapper link__wrapper--small">
                      <span>More Information</span>
                      <span>More Information</span>
                    </div>
                    <div class="link__img link__img--small">
                      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
                    </div>
                  </a>
                <?php endif; ?>
              </div>
            </div>
          </div>
          <?php if( $i == 1 ) : ?>
            <div class="circle circle--3" data-rellax-speed="2"></div>
          <?php elseif( $i == 2 ) : ?>
            <div class="circle circle--4" data-rellax-speed="2"></div>
          <?php elseif( $i == 3) : ?>
            <div class="circle circle--5" data-rellax-speed="1"></div>
          <?php endif; ?>
        </div>
      </section>
    <?php $i++; ?>
    <?php endwhile; ?>
  <?php endif; ?>

<section class="common-mobile">

  <?php if( have_rows('common') ) : ?>
    <?php while( have_rows('common') ) : the_row(); ?>

      <div class="common-mobile__slide">
        <div class="common-mobile__container">
          <div
            class="common-mobile__img"
            style="background-image: url('<?php echo wp_get_attachment_image_url( get_sub_field('image_1', get_the_ID() ), 'common-top', true ); ?>');">
          </div>
          <div class="block block--common">
            <h2 class="ch-title"><?php echo get_sub_field('title'); ?></h2>
            <?php if( get_sub_field('title_blue') ) : ?>
              <h2 class="ch-title ch-title--blue"><?php echo get_sub_field('title_blue'); ?></h2>
            <?php endif; ?>
            <p class="ch-text"><?php echo get_sub_field('text'); ?></p>

            <?php if( get_sub_field('more_information') ) : ?>
              <?php
              if(get_sub_field('open_modal_with_more_information')) {
                $modal = 'open-modal';
              } else {
                $modal = ' ';
              }
              ?>
              <a class="link link--small <?php echo $modal; ?>" href="<?php echo get_sub_field
              ('more_information'); ?>"
                 target="_blank">
                <div class="link__wrapper link__wrapper--small">
                  <span>More Information</span>
                  <span>More Information</span>
                </div>
                <div class="link__img link__img--small">
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
                </div>
              </a>
            <?php endif; ?>

          </div>
        </div>
      </div>

    <?php endwhile; ?>
  <?php endif; ?>

</section>




