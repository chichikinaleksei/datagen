<?php
/**
 * Block Name: Home papers
 */

if(get_field('open_modal_with_more_information')) {
  $opmi = 'open-modal';
} else {
  $opmi = '';
}

?>


<div class="papers">
  <div class="container container--wide">
    <div class="section-header flex row jcsb">
      <h2 class="ch-title ch-title--middle ch-title--bold"><?php echo get_field('title'); ?></h2>
      <a class="link link--smaller link--desktop <?php echo $opmi; ?>" href="<?php echo get_field('more_information')
      ; ?>"
         target="_blank">
        <div class="link__wrapper link__wrapper--smaller">
          <span>More Information</span>
          <span>More Information</span>
        </div>
        <div class="link__img link__img--small">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
        </div>
      </a>
    </div>
  </div>
  <div class="papers__wrapper">

    <?php if( have_rows('papers') ): ?>
      <?php while( have_rows('papers') ): the_row(); ?>
        <div class="paper">
          <div class="paper__wrapper">
            <div class="paper__logo"> <img src="<?php echo get_sub_field('logo'); ?>" alt=""></div>
            <p class="ch-text ch-text--big"><?php echo get_sub_field('text'); ?></p>
            <a class="link link--smaller" href="<?php echo get_sub_field('more_information'); ?>" target="_blank">
              <div class="link__wrapper link__wrapper--smaller">
                <span>More Information</span>
                <span>More Information</span>
              </div>
              <div class="link__img link__img--small">
                <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
              </div>
            </a>
          </div>
        </div>
      <?php endwhile; ?>
    <?php endif; ?>

  </div>
  <a class="link link--smaller link--mobile" href="<?php echo get_field('see_all_articles'); ?>" target="_blank">
    <div class="link__wrapper link__wrapper--smaller">
      <span>More Information</span>
      <span>More Information</span>
    </div>
    <div class="link__img link__img--small">
      <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="<?php echo get_field('title'); ?>">
    </div>
  </a>
</div>
