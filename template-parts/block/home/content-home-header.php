<?php
/**
 * Block Name: Home header
 */

?>
<section class="section fp-auto-height-responsive">
<div
  class="header"
  style=" background-image: linear-gradient(to bottom, rgba(33, 38, 157, 0.65), #000000) ,url(<?php echo
  get_stylesheet_directory_uri() ?>/img/header-bg.png)">
  <div class="navigation flex row aic jcsb">
    <a class="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
      <img src="<?php echo get_field('logo'); ?>" alt="<?php echo get_field('header'); ?>">
    </a>
    <div class="navigation__wrapper">
      <a class="navigation__link" href="#human-simulation-solution">Solutions</a>
      <a class="navigation__link" href="#technology">Tech</a>
      <a class="ch-button ch-button--bordered-white open-modal" href="">Request a Demo</a>
    </div>
  </div>
  <div class="container">
    <div class="header__content">
      <h1 class="ch-title ch-title--big"><?php echo get_field('header'); ?></h1>
      <a class="link open-modal" href="">
        <div class="link__wrapper"><span>Learn More</span><span>Learn More</span></div>
        <div class="link__img">
          <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Data For Understanding The World">
        </div>
      </a>
      <a class="ch-button ch-button--white" href="#contact-us">Contact Us</a>
    </div>
  </div>
  <div class="scroll">
      <span class="scroll__mouse">
        <span></span>
      </span>
  </div>
</div>

