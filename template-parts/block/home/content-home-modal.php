<?php
/**
 * Block Name: Home modal
 */

?>

<div class="modal modal--is-hidden flex aic jcc">
  <div class="modal__close">
    <img src="<?php echo get_stylesheet_directory_uri() ?>/img/close.svg">
  </div>
  <div class="modal__wrapper">
    <div class="modal__form">
      <h1 class="ch-title modal__header"><?php echo get_field('header'); ?></h1>
      <p><?php echo get_field('description'); ?></p>
      [contact-form-7 id="151" title="Modal window"]
    </div>
  </div>
</div>
