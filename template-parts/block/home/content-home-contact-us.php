<?php
/**
 * Block Name: Home contact us
 */

?>
</section>

<section class="section">
<div class="contact-us" id="contact-us">
  <div class="container">
    <div class="contact-us__wrapper flex row jcsb">
      <div class="contact-us__block">
        <h2 class="ch-title ch-title--middle ch-title--bold"><?php echo get_field('title'); ?></h2>
        [contact-form-7 id="152" title="Contact us form"]
      </div>
      <div class="contact-us__block">
        <div class="contact-us__img"> <img src="<?php echo get_field('image'); ?>" alt="Contact Us"></div>
      </div>
    </div>
  </div>
</div>
