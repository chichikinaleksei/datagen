<?php
/**
 * Block Name: Home technology
 */

?>
<section class="section">
<div class="technology" id="technology" >
  <div class="container container--narrow">
    <h2 class="technology__title ch-title ch-title--middle ch-title--bold text-center"><?php echo get_field('title');
    ?></h2>
  </div>

  <?php if( have_rows('technology') ): ?>
    <?php while( have_rows('technology') ): the_row(); ?>
      <div class="technology__wrapper">
        <div class="container container--narrow flex row aic">
          <div class="block block--wide">
            <h2 class="ch-title"><?php echo get_sub_field('title'); ?></h2>
          </div>
          <div class="block block--wide">
            <p class="ch-text"><?php echo get_sub_field('text'); ?></p>

            <?php if( get_sub_field('more_information') ) : ?>
              <?php
              if(get_sub_field('open_modal_with_more_information_button')) {
                $modal = 'open-modal';
              } else {
                $modal = ' ';
              }
              ?>
              <a class="link link--small <?php echo $modal; ?>" href="<?php echo get_sub_field('more_information'); ?>"
                 target="_blank">
                <div class="link__wrapper link__wrapper--small">
                  <span>More Information</span>
                  <span>More Information</span>
                </div>
                <div class="link__img link__img--small">
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
                </div>
              </a>
            <?php endif; ?>

          </div>
        </div>
      </div>
    <?php endwhile; ?>
  <?php endif; ?>

</div>
