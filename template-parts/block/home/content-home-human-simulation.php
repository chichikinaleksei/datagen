<?php
/**
 * Block Name: Home human simulation
 */

if(get_field('open_modal_on_free_academic_data')) {
  $opfa = 'open-modal';
} else {
  $opfa = '';
}
if(get_field('open_modal_on_get_demo_button')) {
  $opdb = 'open-modal';
} else {
  $opdb = '';
}
?>

  <section class="human" id="human-simulation-solution">
    <div class="container flex row aic jcsb human__wrapper">
      <div class="block">
        <h2 class="ch-title"><?php echo get_field('title_'); ?></h2>
        <p class="ch-text"><?php echo get_field('text'); ?></p>
        <a class="link link--small <?php echo $opfa; ?>" target="_blank" href="<?php echo get_field('free_academic_data_link'); ?>">
          <div class="link__wrapper link__wrapper--small">
            <span>Free Academic Data</span>
            <span>Free Academic Data</span>
          </div>
          <div class="link__img link__img--small">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/right-arrow-white.svg" alt="Human Simulation Solution">
          </div>
        </a>
        <a class="ch-button ch-button--blue button--middle <?php echo $opdb; ?>" target="_blank" href="<?php echo
        get_field('get_demo_link'); ?>">Get a
          Demo</a>
      </div>
      <div class="human__img">
        <img src="<?php echo get_field('image'); ?>" alt="Human Simulation Solution">
        <div class="circle circle--1" data-rellax-speed="1.5"></div>
        <div class="circle circle--2" data-rellax-speed="1"></div>
      </div>
    </div>
  </section>
</section>