<?php
/**
 * Block Name: Home realism
 */

?>

<div class="realism">
  <h2 class="ch-title ch-title--middle ch-title--bold text-center"><?php echo get_field('title'); ?></h2>
  <div class="container container--narrow">
    <div class="realism__wrapper flex row jcsb">

      <?php $i = 1; ?>
      <?php if( have_rows('blocks') ): ?>
        <?php while( have_rows('blocks') ): the_row(); ?>
          <div class="realism__item flex column aic jcc">
            <div class="realism__item-img">
              <img src="<?php echo get_sub_field('icon'); ?>" alt="">
            </div>
            <p class="ch-text"><?php echo get_sub_field('title'); ?></p>
          </div>
        <?php if($i !== 3) : ?>
          <div class="realism__item-separator"></div>
        <?php endif; ?>
          <?php $i++; ?>
        <?php endwhile; ?>
      <?php endif; ?>

    </div>
    <div class="realism__img"> <img src="<?php echo get_field('image'); ?>" alt=""></div>
  </div>
  <div class="circle circle--6"></div>
  <div class="circle circle--7"></div>
</div>
