<?php
/**
 * Block Name: Home split
 */


?>
<div class="split-section">
  <div class="split-section__container">
    <div class="split-section__wrapper cocoen">
      <img src="<?php echo get_field('image_1'); ?>">
      <img src="<?php echo get_field('image_2'); ?>">
    </div>
  </div>
</div>
