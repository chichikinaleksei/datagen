<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package datagen
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ) ?>">
	<title><?php bloginfo( 'name' ); ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE = edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="keywords" content="">

  <!--Preload Fonts-->
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/fonts/Montserrat-Bold.woff2" as="font"
        type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/fonts/Montserrat-Medium.woff2" as="font"
        type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/fonts/Montserrat-Regular.woff2" as="font"
        type="font/woff2" crossorigin="anonymous">
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/fonts/Montserrat-SemiBold.woff2" as="font"
        type="font/woff2" crossorigin="anonymous">
  <!--Preload scripts-->
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/js/main.js" as="script">
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/js/libs.min.js" as="script">
  <!--Preload Images-->
  <link rel="preload" href="<?php echo get_stylesheet_directory_uri() ?>/img/header-bg.png" as="image">
  <link rel="preload" href="<?php echo WP_CONTENT_URL ?>/uploads/2019/09/human-simulation.png" as="image">
  <link rel="preload" href="<?php echo WP_CONTENT_URL ?>/uploads/2019/09/drone-simulation-1.png" as="image">
  <link rel="preload" href="<?php echo WP_CONTENT_URL ?>/uploads/2019/09/drone-simulation-2.png" as="image">

	<?php wp_head(); ?>

</head>
<body class="datagen">

<div class="navbar is-hidden">
  <a href="" class="navbar__link"></a>
  <a href="" class="navbar__link"></a>
  <a href="" class="navbar__link"></a>
</div>

