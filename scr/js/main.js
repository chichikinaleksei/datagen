// Form labels animation
let fields = document.querySelectorAll('.field');

fields.forEach(field => {
    field.onfocus = () => {
        let parent = field.parentElement.parentElement;
        parent.firstElementChild.classList.add('form__label--is-active');
    };
    field.onblur = () => {
        if (field.value == '') {
            let parent = field.parentElement.parentElement;
            parent.firstElementChild.classList.remove('form__label--is-active');
        }
    };
});


// Open close modal
let openModal = document.querySelectorAll('.open-modal');
let closeModal = document.querySelector('.modal__close');
let modal = document.querySelector('.modal');

if(modal) {
    openModal.forEach(el => {
        el.addEventListener('click', e => {
            e.preventDefault();
            modal.classList.remove('modal--is-hidden');
        })
    });
    closeModal.addEventListener('click', e => {
        e.preventDefault();
        modal.classList.add('modal--is-hidden');
    });
}


// Sliders
if (window.matchMedia("(max-width: 770px)").matches) {
    let common = tns({
        container: '.common-mobile',
        controls: false,
        speed: 600,
        items: 3,
        slideBy: 1,
        center: true,
        autoplay: false,
        mouseDrag: true,
        autoWidth: true,
        nav: false,
        autoplayHoverPause: true,
        loop: false,
        startIndex: 1,
        preventScrollOnTouch: 'auto'
    });
}
if (window.matchMedia("(min-width: 770px)").matches) {

    let papers = tns({
        container: '.papers__wrapper',
        controls: false,
        speed: 600,
        items: 3,
        slideBy: 1,
        center: true,
        autoplay: false,
        mouseDrag: true,
        autoWidth: true,
        nav: false,
        autoplayHoverPause: true,
        loop: false,
        startIndex: 1,
        preventScrollOnTouch: 'auto'
    });

}

let articles = tns({
    container: '.articles__wrapper',
    controls: false,
    speed: 700,
    items: 3,
    slideBy: 1,
    center: true,
    autoplay: false,
    mouseDrag: true,
    autoWidth: true,
    nav: false,
    autoplayHoverPause: true,
    loop: false,
    startIndex: 1,
    preventScrollOnTouch: 'auto'
});




// Split screen animation
let splitSection = document.querySelector('.split-section__wrapper');
if(splitSection) {
    new Cocoen(document.querySelector('.split-section__wrapper'));
}


// Fade up on images
AOS.init();

// Navbar
let navbar = document.querySelector('.navbar');
let commonSections = document.querySelectorAll('.common');
let commonOffset = document.querySelector('#common-1').offsetTop;
let technologyOffset = document.querySelector('#technology').offsetTop;

let linksArr = '<a class="navbar__link" href="#human-simulation-solution"></a>';

commonSections.forEach(section => {
   let sectionId = section.id;
   linksArr += `<a class="navbar__link" href="#${sectionId}"></a>`;
});
linksArr += '<a class="navbar__link" href="#technology"></a>';
navbar.innerHTML = linksArr;

window.addEventListener('scroll', () => {
    if(window.pageYOffset >= commonOffset && window.pageYOffset < technologyOffset) {
        navbar.classList.remove('is-hidden');
    } else {
        navbar.classList.add('is-hidden');
    }
});


// Circle parallax
var rellax = new Rellax('.circle', {
    center: false,
    wrapper: null,
    round: true,
    vertical: true,
    horizontal: false
});




