<?php

add_action('acf/init', 'home_acf_init');
function home_acf_init() {
  if( function_exists('acf_register_block') ) {
    acf_register_block(array(
      'name'				=> 'home-header',
      'title'				=> __('Home header'),
      'description'		=> __('A custom home header block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-header' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-human-simulation',
      'title'				=> __('Home human simulation'),
      'description'		=> __('A custom home human simulation block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-human-simulation' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-common',
      'title'				=> __('Home common'),
      'description'		=> __('A custom home common block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-common' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-technology',
      'title'				=> __('Home technology'),
      'description'		=> __('A custom home technology block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-technology' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-split',
      'title'				=> __('Home split'),
      'description'		=> __('A custom home split block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-split' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-realism',
      'title'				=> __('Home realism'),
      'description'		=> __('A custom home realism block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-realism' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-help',
      'title'				=> __('Home help'),
      'description'		=> __('A custom home help block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-help' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-papers',
      'title'				=> __('Home papers'),
      'description'		=> __('A custom home papers block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-papers' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-articles',
      'title'				=> __('Home articles'),
      'description'		=> __('A custom home articles block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-articles' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-contact-us',
      'title'				=> __('Home contact-us'),
      'description'		=> __('A custom home contact-us block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-contact-us' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-footer',
      'title'				=> __('Home footer'),
      'description'		=> __('A custom home footer block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-footer' ),
    ));
    acf_register_block(array(
      'name'				=> 'home-modal',
      'title'				=> __('Home modal'),
      'description'		=> __('A custom home modal block.'),
      'render_callback'	=> 'home_acf_block_render_callback',
      'category'			=> 'datagen-blocks',
      'icon'				=> 'star-filled',
      'keywords'			=> array( 'home-modal' ),
    ));

  }
}

function home_acf_block_render_callback( $block ) {
  $slug = str_replace('acf/', '', $block['name']);
  if( file_exists( get_theme_file_path("/template-parts/block/home/content-{$slug}.php") ) ) {
    include( get_theme_file_path("/template-parts/block/home/content-{$slug}.php") );
  }
}